Go Monero RPC Client
====================
This repo forks the [Go Monero RPC Client](https://github.com/monero-ecosystem/go-monero-rpc-client).
We added some golang RPC calls to support [Thorchain Customised Monero](https://gitlab.com/thorchain/tss/monero-sign).

This repository provides the methods for golang client (e.g., [thorchain TSS](https://gitlab.com/thorchain/tss/go-tss))
to invoke the interfaces of Monero Wallet RPC daemon.

## Interface Updates

To support thorchain customised Monero, we added the following interfaces.

```go	
// Export the node's multisig pubkeys for signing
ExportSigPubKey() (resp *ResponseExportSigPubkey, err error)
// Sign the transaction in prarallel and return the share for accumulation
SignMultisigParallel(*RequestSignMultisigParallel) (*ResponseSignMultisigParallel, error)
// Accumulate the signatures
AccuMultisig(req *RequestAccuMultisig) (resp *ResponseAccuMultisig, err error)
// CheckTransaction chect k whether the transaction we received has the same destination and amount
CheckTransaction(req *RequestCheckTransaction) (resp *ResponseCheckTransaction, err error)
// create the pool wallet
SavePoolWallet(*RequestSavedPoolWallet) (*ResponseSavePoolWallet, error)
```

These interfaces are wraps of the RPC interfaces implemented in  [Thorchain Customised Monero](https://gitlab.com/thorchain/tss/monero-sign).
