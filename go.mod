module gitlab.com/thorchain/tss/monero-wallet-rpc

go 1.15

require (
	github.com/gorilla/rpc v1.2.0
	github.com/stretchr/testify v1.7.0
)
